package model;
/**
 *  This class represents a spending account.
 * @author Paula Hreniuc
 *
 */
public class SpendingAccount extends Account {
	private final double limit = 8000;
	private double credit = 0;
	//private int interest = 10;
	
	/**
	 *  Creates a spending account.
	 * @param p the person who has the account
	 * @param amount the initial amount of money in the account
	 * @param id the id of the account
	 * @pre id > 0 && amount >= 0 && p != null
	 */
	public SpendingAccount(Person p, double amount, int id) {
		super (p, amount, id);
	}
	
	/**
	 * This method performs a withdrawal.
	 * @param sumToBeWidrawen the amount to be taken out
	 * @return 1 in case of success, 0 otherwise
	 */
	public int withdraw(double sumToBeWidrawen ) {
		assert sumToBeWidrawen > 0;
		if(sumToBeWidrawen <= amount) {
			super.withdraw(sumToBeWidrawen);
			return 1;
		}
		else if(sumToBeWidrawen <= (limit - credit) + amount){
			credit = credit + sumToBeWidrawen - amount;
			amount = 0;
			return 1;
		}
		else
			return 0;
	}
	
	/**
	 * returns the unique identifier of the account
	 * @return
	 */
	public int getID() {
		return cnpID;
	}
	
	/**
	 * return the details about the spending account
	 * @return 
	 */
	public String toString() {
		assert p!= null;
		return "Spending account:" + p.toString() + "  amount: " + amount +" and credit of: " + credit  + " id = " + cnpID;
	}
	
	public String accountInfoID() {
		return "Account with id:" + cnpID;
	}
}
