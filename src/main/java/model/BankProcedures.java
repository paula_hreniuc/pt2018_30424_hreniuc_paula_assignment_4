package model;

import java.util.ArrayList;

import model.Account;

public interface BankProcedures {
	
   public void addAccount(String cnp, Account account);
   public void populate(String filename);
   public  ArrayList<Account> search(String key);
   public void save(String fileName);
   public void removeClient(String cnp);
   public void removeAccount(String cnp, int idAcc);
   public void removePerson(String cnp);
   public void editAccount(String cnp, int id, float sum);
   public void addToAccount(String cnp, int id, float sum);
   public void WithdrawFromAccount(String cnp, int id, float amountW);
   public void editPerson(String cnp, Person p) ;
   public String[][] allAccRet();
   public String[][] allPersons();
   public String getAccounts(String cnp);


}
