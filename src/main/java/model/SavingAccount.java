package model;
/**
 * This class represents a saving account.
 * @author Paula Hreniuc
 *
 */
public class SavingAccount extends Account {

	private final int daysMonth = 30;
	private int interest = 2;
	private int lastTime = 0;
	
	/**
	 * Creates a saving account.
	 * @param p
	 * @param amount
	 * @param id
	 * @pre p != null && id > 0 && amount >= 0
	 */
	public SavingAccount(Person p, double amount, int id) {
		super(p, amount, id);		
	}
	

/*	public int withdraw(double sumToBeWithdrawn, int date) {
		assert sumToBeWithdrawn >= 0 && date >0;
		if (sumToBeWithdrawn <= amount) {
			amount = amount - sumToBeWithdrawn;
			if(date-lastTime < this.daysMonth){
				interest = 1;
			}
			lastTime = date ;
			return 1;
		}
		else 
			return 0;
		}*/
	/**
	 *  This method performs a withdrawal.
	 * @param sumToBeWithdrawn the amount to be taken out
	 * @return 1 if successful, 0 otherwise
	 */
	public int withdraw(double sumToBeWithdrawn) {
		assert sumToBeWithdrawn >= 0;
		if (sumToBeWithdrawn <= amount) {
			amount = amount - sumToBeWithdrawn;
			System.out.println("withdrawn " + amount + (interest/100)*amount);
			return  (int) (amount + (interest/100)*amount);
		}
		else 
			return 0;
		}
	
	/**
	 * returns the unique identifier of the account
	 * @return
	 */
	public int getID() {
		return cnpID;
	}
	
	/**
	 * returns the detailes about a saving account 
	 * @return 
	 */
	public String toString() {
		assert p!= null;
		return "Saving account:" + p.toString() + "  amount: " + amount + " id = " + cnpID;
	}
	
	public String accountInfoID() {
		return "Account with id:" + cnpID;
	}
	
	public static void main (String args[]) {
		Person p = new Person("1234567891234", "paula", "hre", "0754637729");
		SavingAccount sv = new SavingAccount(p, 400, 1);
		System.out.println( sv.toString());
		sv.withdraw(250);
		System.out.println( sv.toString());

	}
}
