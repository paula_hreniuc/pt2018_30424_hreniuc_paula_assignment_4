package model;

import java.io.Serializable;
/**
 * This class represents a person's profile, a customer of the bank
 * @author Paula Hreniuc
 *
 */
public class Person implements Serializable {

	private String CNP;
	private String firstName;
	private String lastName;
	private String phone;
	
	/**
	 * 
	 * @param CNP unique identifier within a bank
	 * @param firstName
	 * @param lastName
	 * @param phone
	 */
	public Person(String CNP, String firstName, String lastName, String phone) {
		this.CNP = CNP;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}
	
	/**
	 * 
	 * @param CNP unique identifier within a bank 
	 */
	public Person(String CNP) {
		this.CNP = CNP;
	}

	/**
	 * @return Returns a string which contains information regarding this person.
	 */
	public String toString() {
		return "The customer with CNP " + CNP + " has first name " + firstName + " and last name " + lastName + " and has phone " + phone;
	}
	
	/**
	 * 
	 * @return cnp
	 */
	public String getCNP() {
		return this.CNP;
	}
	
	/**
	 * 
	 * @param cnp
	 */
	public void setCNP(String cnp) {
		this.CNP = cnp;
	}
	
	/**
	 * returns the first name of the person
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	/**
	 * 
	 * @param firstName
	 */
	public void setFName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * returns the last name of the person
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * 
	 * @param lastName
	 */
	public void setLName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * returns the phone number of the person
	 * @return
	 */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * 
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
}
