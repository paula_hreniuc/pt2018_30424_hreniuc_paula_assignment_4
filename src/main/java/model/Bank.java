package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/*
 * 3. An Observer DP will be defined and implemented. It will notify the account main holder about any account related operation.
 */
/**
 * This class represents a bank's database of accounts.
 * @author Paula Hreniuc
 * @invariant isWellFormed()
 */
public class Bank implements BankProcedures, Serializable {

	public boolean isWellFormed() {
		boolean n;
		if(bank != null)
			n = true;
		else
			n = false;
		
		return n;
	}
	private HashMap<String, ArrayList<Account>> bank;

	/**
	 * Creates a bank's database of accounts.
	 */
	public Bank() {
		bank = new HashMap<String, ArrayList<Account>>();
	}
	
	/**
	 * @pre !cnp.equals("") && cnp.length() == 13 && account !=null
	 */
    public void addAccount(String cnp, Account account) {
    	assert !cnp .equals("") && cnp.length() ==13 && account != null;
    	assert isWellFormed();
    	ArrayList<Account> values = bank.get(cnp);
    	if(values == null) {
    		values = new ArrayList<Account>();
    	}
    	if(!values.contains(account)) {
    		values.add(account);
    		bank.put(cnp, values);
    		System.out.println("Succesfuly added " + cnp);
    	}
    	else {
    		System.out.println("fail add " + cnp);

    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to populate the bank with the data from the given file
     */
    public void populate(String filename) {
    	try {
    		FileInputStream fs = new FileInputStream(filename);
    		ObjectInputStream object = new ObjectInputStream(fs);
    		bank = (HashMap<String, ArrayList<Account>>)object.readObject();
    	}
    	catch (Exception e) {
    	e.printStackTrace();
    	}
    	
    	System.out.println("populated");
    }
    
    /**
     * @pre key != null && key.length == 13
     */
    public  ArrayList<Account> search(String key){
    	assert isWellFormed();
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(key)) {
    			return bank.get(key);
    		}
    	}
    	assert isWellFormed();
    	return null;
    }
    
    /**
     * this method helps us save new data to the given file 
     */
    public void save(String fileName) {
    	try {
    		FileOutputStream fs = new FileOutputStream(fileName);
    		ObjectOutputStream object = new ObjectOutputStream(fs);
    		object.writeObject(bank);
    		fs.close();
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	System.out.println("Done writing\n");
    }
    
    /**
     * @pre cnp != null && cnp.length() == 13
	 * @post getSize() == getSize()@pre + 1
     */
    public void removeClient(String cnp) {
    	
    	assert cnp != null && cnp.length() == 13;
    	assert isWellFormed();
    	Enumeration<String> keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		int size = bank.keySet().size();
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			bank.remove(cnp);
    			int sizePost = bank.keySet().size();
    			assert sizePost == size + 1;
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to remove an account given a persons cnp and the unique identifier of the account that we want to remove
     */
    public void removeAccount(String cnp, int idAcc) {//Account account
    	assert isWellFormed();
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = bank.get(cnp);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				Account a = iter.next();
    				if(a.getCnpID() == idAcc){//account.equals(a)
    					iter.remove();
    					break;
    				}
    			}
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method to remove a person 
     */
    public void removePerson(String cnp) {
    	assert isWellFormed();
    	//Person deleted = new Person(" ", " ", " ", " ");
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = bank.get(cnp);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				//we remove all the person accounts
    				Account a = iter.next();
    				iter.remove();
    			}
    			acc = search(myKey);
    			//Person p = acc.get(0).getPerson();
    			//p = deleted;
    			
    			//acc.get(0).getPerson().setCNP("");
    			//acc.get(0).getPerson().setFName("");
    			//acc.get(0).getPerson().setLName(" ");
    			//acc.get(0).getPerson().setPhone(" ");
    			
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to edit a specific account giving the persons cnp, the id of the account that we want to edit and new sum 
     */
    public void editAccount(String cnp, int id, float sum) {//  Account account
    	assert isWellFormed();
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = bank.get(cnp);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				Account a = iter.next();
    				if(a.getCnpID() == id){//account.equals(a)
    					a.amount = sum;
			
    				}
    			}
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to add a sum to an already existing account  
     */
    public void addToAccount(String cnp, int id, float sum) {//  Account account
    	assert isWellFormed();
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = bank.get(cnp);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				Account a = iter.next();
    				if(a.getCnpID() == id){//account.equals(a)
    					a.amount = a.amount + sum;
			
    				}
    			}
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to withdraw a specific amount from an existing account
     */
    public void WithdrawFromAccount(String cnp, int id, float amountW) {//  Account account
    	assert isWellFormed();
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = bank.get(cnp);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				Account a = iter.next();
    				if(a.getCnpID() == id){//account.equals(a)
    					//a.amount = a.amount + sum;
    					a.withdraw(amountW);
    				}
    			}
    		}
    	}
    	assert isWellFormed();
    }
    
    /**
     * method used to edit the informations about a client/person that is found in the bank's system
     */
    public void editPerson(String cnp, Person p) {
    	assert isWellFormed();
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		if(myKey.equals(cnp)) {
    			acc = search(myKey);
    			//Person toBeEddited = acc.get(0).getPerson();
    			//toBeEddited = p;
    			acc.get(0).getPerson().setCNP(p.getCNP());
    			acc.get(0).getPerson().setFName(p.getFirstName());
    			acc.get(0).getPerson().setLName(p.getLastName());
    			acc.get(0).getPerson().setPhone(p.getPhone());
    			}
    		}
    	assert isWellFormed();
    }
    
    /**
     * @return method returns a String[][] containing the informations about all the accounts that are found in the bank 
     */
    public String[][] allAccRet() {
    	assert isWellFormed();
    	String [][] all = new String[100][7];//[2];
    	int i = 0;
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		//if(myKey.equals(cnp)) 
    		{
    			acc = bank.get(myKey);
    			Iterator<Account> iter = acc.iterator();
    			while(iter.hasNext()) {
    				Account a = iter.next();
    				//all[i][0] = a.toString();
    				//all[i][1] = myKey;
        			acc = search(myKey);
        			
    				all[i][0] = acc.get(0).getPerson().getCNP();
    				all[i][1] = acc.get(0).getPerson().getFirstName();
    				all[i][2] = acc.get(0).getPerson().getLastName();
    				all[i][3] = acc.get(0).getPerson().getPhone();
    				all[i][4] = a.toString();
    				all[i][5] = Double.toString( a.getAmount());
    				all[i][6] = Integer.toString( a.getCnpID());
    				i++;
    			}
    			i++;
    		}
    	}
    	assert isWellFormed();
    	return all;
    }
    
    /**
     * @return method returns a String[][] containing the informations about all the persons that are found in the bank 
     */
    public String[][] allPersons() {
    	assert isWellFormed();
    	String [][] allp = new String[100][4];
    	int i = 0;
    	ArrayList<Account> acc;
    	Enumeration<String>keys = Collections.enumeration(bank.keySet());
    	while(keys.hasMoreElements()) {
    		String myKey = (String)(keys.nextElement());
    		//if(myKey.equals(cnp)) 
    		{
    			acc = search(myKey);
    			if (acc != null){
    	         	for(Account f: acc){
    	             	//System.out.println("serached for " + f.getPerson());// + f.toString());
    	             	Person apers = f.getPerson();
    	             	System.out.println("serached for person !!! " + acc.get(0).getPerson().getCNP() + " " +apers.getCNP() + " " + apers.getFirstName());
    	             	allp[i][0] = apers.getCNP();
    	    			allp[i][1] = apers.getFirstName();
    	    			allp[i][2] = apers.getLastName();
    	    			allp[i][3] = apers.getPhone();
    	    			i++;
    	    			break;
    	         	}
    	         }
    			/*acc = search(myKey);
	    			allp[i][0] = myKey;// acc.get(0).getPerson().getCNP();
	    			allp[i][1] = acc.get(0).getPerson().getFirstName();
	    			allp[i][2] = acc.get(0).getPerson().getLastName();
	    			allp[i][3] = acc.get(0).getPerson().getPhone();
	    			i++;
	    		System.out.println(allp[i][0] + allp[i][1]+allp[i][2]+allp[i][3]);*/
    		}
    	}
    	assert isWellFormed();
    	return allp;
    }
    
    /**
     * @return methods return as String all the accounts a person has 
     */
    public String getAccounts(String cnp){
    	assert cnp!= null && cnp.length() == 13;
    	assert isWellFormed();
    	StringBuilder res = new StringBuilder();
    	ArrayList<Account> acc;
    	acc = this.search(cnp);
    	if(acc != null) {
    		for(Account a: acc) {
    			res = res.append(a.toString());
    			System.out.println(a.toString()+"\n");//
    			res.append(System.getProperty("line.separator"));
    		}
    	}
    	assert isWellFormed();
    	return res.toString();
    }
    
    

    
    //update/edit an account
    //update/edit a person
    //add a person from gui to file// make anouther constructor where i receive a person or in the same contructor change
    //functions for Jtables
    
    public static void main(String[] args) {
    	
    	Bank b = new Bank();
		b.populate("bank.txt");
        Person p = new Person("2941711125781", "Hreniuc", "Paula", "0745352271");
        SpendingAccount sa = new SpendingAccount(p, 1000, 6);
        SavingAccount sv = new SavingAccount(p, 5000, 8);
        SavingAccount sv2 = new SavingAccount(p, 5000, 10);
        
        b.addAccount("2941711125781", sa);
        b.addAccount("2941711125781", sv);
        b.addAccount("2941711125781", sv2);


        ArrayList<Account> ac;
        ac = b.search("2941611125781");
        if (ac != null){
        	for(Account a: ac){
            	System.out.println(a.toString());
            }
        }
        else
        	System.out.println("Person not in bank");
    	
    	Person p1 = new Person("5941711125781","Ana", "Pop", "0754636672");
    	SavingAccount a = new SavingAccount(p1, 8000, 1);
    	b.addAccount("5941711125781", a); 

    	SavingAccount a2 = new SavingAccount(p1, 82900, 2);
    	b.addAccount("5941711125781", a2); 
    	
    	Person p2 = new Person("1234567887234","AnaMaria", "Mariginean", "0754556672");
    	SavingAccount acc = new SavingAccount(p2, 8000, 3);
    	b.addAccount("1234567887234", acc);
    	
    	Person p3 = new Person("1231231231233", "Aurica", "Florea", "07445637721" );
    	SpendingAccount a3 = new SpendingAccount(p3, 4579, 4);
    	b.addAccount("1231231231233", a3);
    	
        b.save("bank.txt");      

    	System.out.println("printing");
    	b.getAccounts("2941711125781");
        b.getAccounts("1234567887234");
    	b.getAccounts("1231231231233");
    	b.getAccounts("5941711125781");


    	System.out.println("removed");
    	b.removeAccount("5941711125781", 2);//a2
    	b.getAccounts("5941711125781");
    	
    	System.out.println("search");
    	 ArrayList<Account> ac2;
         ac2 = b.search("2941611125781");
         if (ac2 != null){
         	for(Account f: ac2){
             	System.out.println("serached" + f.toString());
             }
         }
         else
         	System.out.println("Person not in bank");
         
         ArrayList<Account> ac4;
         ac4 = b.search("1234567887234");
         if (ac4 != null){
         	for(Account f: ac4){
             	System.out.println("serached for " + f.getPerson());// + f.toString());
             	Person apers = f.getPerson();
             	System.out.println("serached for person !!! " + ac4.get(0).getPerson().getCNP() + " " +apers.getCNP() + apers.getFirstName());
             }
         }
         else
         	System.out.println("Person not in bank");
         
         System.out.println("found\n");
         ArrayList<Account> ac3;
         ac3 = b.search("1234567887234");
         if (ac3 != null){
         	for(Account g: ac3){
             	System.out.println(g.toString());
             }
         }
         else
         	System.out.println("Person not in bank"); 	
         
         System.out.println("\nedit\n");
         b.editAccount("2941711125781",8, 1863);
         b.getAccounts("2941711125781");
         
     	System.out.println("removed");
         b.removeAccount("2941711125781", 6);//sa
         b.getAccounts("2941711125781");
         
         System.out.println("Remove person");
         b.removePerson("2941711125781");
         b.getAccounts("2941711125781");
         
         b.allPersons();
         

    }
}
