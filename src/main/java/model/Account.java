package model;

import java.io.Serializable;
/**
 * 
 * @author Paula Hreniuc
 * This class represents the account one person can have in a bank.
 */
public class Account implements Serializable {
	protected Person p;
	protected double amount;
	protected int cnpID;
	
	public Account() {
		
	}
	/**
	 * 
	 * @param p is a Person
	 * @param amount
	 * @param id is a unique identifier of the specific account created
	 */
	public Account(Person p, double amount, int id) {
		this.p = p;
		this.amount = amount;
		this.cnpID = id;		
	}
	/**
	 * 
	 * @param sumToWithdraw
	 * @return int 1 on succes and 0 on fail
	 */
	public int withdraw(double sumToWithdraw) {
		if (amount >= sumToWithdraw ) {
			amount = amount - sumToWithdraw;
			return 1;
		}
		return 0;
	}
	/**
	 * This methods adds to an account the sum specified as parameter
	 * @param sumToBeDeposited
	 */
	public void deposit(double sumToBeDeposited) {
		assert sumToBeDeposited >0;
		amount = amount + sumToBeDeposited;
		assert amount == amount +sumToBeDeposited;
	}

	public boolean equals(Object e) {
		assert e != null;
		if(e instanceof Account) {
			Account a = (Account)e;
			if (a.getCnpID() == this.cnpID) {
				return true;
			}
			else
				return false;
		}
		else {
			return false;
		}
	}
	/**
	 * 
	 * @return as String the id of the account !!Be aware the cnpID that is returned is NOT the CNP, the name is not is wrongly put 
	 */
	public String accountInfoID() {
		return "Account with id:" + cnpID;
	}
	
	/**
	 * 
	 * @return returns a Person object
	 */
	public Person getPerson() {
		assert p!= null;
		return this.p;
	}
	
	/**
	 * 
	 * @return the unique identifier of the account which doesn't have to do anything with the the cnp  !!! be aware
	 */
	public int getCnpID() {
		return cnpID;
	}
	/**
	 * 
	 * @return returns as double the amount of money the account has
	 */
	 public double getAmount() {
		 return amount;
	 }
}
