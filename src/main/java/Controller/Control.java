 package Controller;

import model.Bank;
import views.LoginInBank;
import views.ViewBank;
import views.WindowAccount;
import views.WindowPerson;
/**
 * Created the starting view, the Log In grafical userinterfaces tht gives access to the bank application and operations
 * @author Paula Hreniuc
 *
 */
public class Control {

	public static Bank bank;
	public LoginInBank lg ;

	
	public Control() {
		lg = new LoginInBank();

	}
	public static void main(String[] args) {
		Control c = new Control();
	}
}
