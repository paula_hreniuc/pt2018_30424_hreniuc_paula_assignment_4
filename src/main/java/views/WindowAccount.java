package views;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
/**
 * Creates the view for the Account operations
 * @author Paula Hreniuc
 *
 */
public class WindowAccount extends JFrame {

	private static final long serialVersionUID = 1L;
	
	JFrame frame  = new JFrame ("Accounts Operations");
	JPanel panel = new JPanel();
	JButton AddAccount = new JButton("Add New Account");
	JButton EditAccount = new JButton("Edit Account");
	JButton DeleteAccount = new JButton("Delete Account");
	JButton ViewAllAccounts = new JButton("View All Accounts");
	JRadioButton SavingAcc = new JRadioButton("Saving Account");
	JTextField deposit = new JTextField("Sum Deposit");
	JTextField withdrawel = new JTextField("Sum Withdrawel");
	JRadioButton SpendingAcc = new JRadioButton("Spending Account");
	JTextField depotS = new JTextField("Deposits");
	JTextField drawelS = new JTextField("Withdrawels");
	JLabel lb = new JLabel("CNP:", JLabel.LEFT);
	JLabel lb2 = new JLabel(" ");
	JLabel lb3 = new JLabel(" ");
	JTextField cnp = new JTextField("CNP");
	JButton Search = new JButton("Search Person");
	JButton AddSum = new JButton("Add sum");
	JButton Withdraw = new JButton("Withdraw");
	JTextField idToEdit = new JTextField("Id to be edited");

	
	Bank bank = new Bank();
	//private String currentCNP;
	private int globalId = 1;
	
	//WindowPerson persons = new WindowPerson();

	//ArrayList<Account> accounts = new ArrayList<Account>();
	public String[][] allAccounts = new String[100][3];
	public int j = 0;

/**
 * Creates the frame and adds action listeners
 */
	public WindowAccount() {
		frame.add(panel);
		//frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 730);
		frame.setVisible(true);
		
		initialize();
		
		//add action listeners
		AddAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					
					ArrayList<Account> arr = bank.search(getCNP());
					Person p = arr.get(0).getPerson();
					
					if (SavingAcc.isSelected()){
						allAccounts[j][0] = getCNP();
						allAccounts[j][1] = String.valueOf(getSumDeposit());
						allAccounts[j][2] = "saving account";
						j++;
						
						SavingAccount sa = new SavingAccount(p, getSumDeposit(), globalId);
						System.out.println(sa.toString());
						globalId += 1;
						bank.addAccount(getCNP(), sa);
						bank.save("bank.txt");
					}
					else if (SpendingAcc.isSelected()){
						allAccounts[j][0] = getCNP();
						allAccounts[j][1] = String.valueOf(getADeposit());
						allAccounts[j][2] = "spending account";
						j++;
						
						SpendingAccount sp = new SpendingAccount(p, getADeposit() , globalId);
						System.out.println(sp.toString());
						globalId += 1;
						bank.addAccount(getCNP(), sp);
						bank.save("bank.txt");
					}
					
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Search.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){
				String cnp = getCNP();
				if(cnp.length() != 13) {
					JOptionPane.showMessageDialog(frame,"The cnp doesn't have 13 numbers", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					try{
						ArrayList<Account> ac4;
						ac4 = bank.search(cnp);
				        if (ac4 == null) {
							JOptionPane.showMessageDialog(frame,"Person with this cnp not in bank", "Error", JOptionPane.ERROR_MESSAGE);
				        }
				        else { 
							JOptionPane.showMessageDialog(frame,"Person is in bank", "Message", JOptionPane.INFORMATION_MESSAGE);
				        }
						
					}
					catch(Exception e){
						System.out.println(e.getMessage());
					}
				}
			}
		});
		
		ViewAllAccounts.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					/*String[] columnsNames = {"cnp", "in account", "type"};
					VJTable qui = new VJTable(allAccounts, columnsNames);
					//qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui.setSize(600,200);
					qui.setVisible(true);	*/
					
					bank.populate("bank.txt");
					String[] columnsNames2 = {"cnp", "first name", "last name", "phone","info","amount", "index"};//"amount",
					String[][] all = bank.allAccRet();
					VJTable qui2 = new VJTable(all, columnsNames2);
					qui2.setSize(600,200);
					qui2.setVisible(true);					
					
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		EditAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String cnpEditAcc = getCNP();
			        ArrayList<Account> acc;
					acc = bank.search(cnpEditAcc);
			         if (acc != null){
			         	for(Account g: acc){
			             	System.out.println(g.toString());
			             }
			         }
			         else {
							JOptionPane.showMessageDialog(frame,"Person with this cnp not in bank", "Error", JOptionPane.ERROR_MESSAGE);
			         }
			         
			        if(SavingAcc.isSelected()) {
						bank.editAccount(cnpEditAcc, getIdToEdit(), getSumDeposit());	
			        }
			        else if(SpendingAcc.isSelected()) {
			        	bank.editAccount(cnpEditAcc, getIdToEdit(), getADeposit());	

			        }	        
					bank.save("bank.txt");
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		DeleteAccount.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String cnpDelAcc = getCNP();
			        ArrayList<Account> acc;
					acc = bank.search(cnpDelAcc);
			         if (acc != null){
			         	for(Account g: acc){
			             	System.out.println(g.toString());
			             }
			         }
			         else {
							JOptionPane.showMessageDialog(frame,"Person with this cnp not in bank", "Error", JOptionPane.ERROR_MESSAGE);
			         }
			         
			       // Account delete = null;
			        if(SavingAcc.isSelected()) {
			        	//delete = new Account(acc.get(0).getPerson(), getSumDeposit(), globalId);
			        	bank.removeAccount(cnpDelAcc, getIdToEdit());// delete);
						bank.save("bank.txt");
			        }
			        else if(SpendingAcc.isSelected()) {
			        	// = new Account(acc.get(0).getPerson(), getADeposit(), globalId);
			        	bank.removeAccount(cnpDelAcc,getIdToEdit());// delete);
						bank.save("bank.txt");
			        }	        
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		AddSum.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String cnpEditAcc = getCNP();
			        ArrayList<Account> acc;
					acc = bank.search(cnpEditAcc);
			         if (acc != null){
			         	for(Account g: acc){
			             	System.out.println(g.toString());
			             }
			         }
			         else {
							JOptionPane.showMessageDialog(frame,"Person with this cnp not in bank", "Error", JOptionPane.ERROR_MESSAGE);
			         }
			         
			        if(SavingAcc.isSelected()) {
						JOptionPane.showMessageDialog(frame,"We can not add to a saving account", "Error", JOptionPane.ERROR_MESSAGE);
			        }
			        else if(SpendingAcc.isSelected()) {
			        	bank.addToAccount(cnpEditAcc, getIdToEdit() , getADeposit());	

			        }	        
					bank.save("bank.txt");
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Withdraw.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String cnpEditAcc = getCNP();
			        ArrayList<Account> acc;
					acc = bank.search(cnpEditAcc);
			         if (acc != null){
			         	for(Account g: acc){
			             	System.out.println(g.toString());
			             }
			         }
			         else {
							JOptionPane.showMessageDialog(frame,"Person with this cnp not in bank", "Error", JOptionPane.ERROR_MESSAGE);
			         }
			         
			        if(SavingAcc.isSelected()) {
			        	bank.WithdrawFromAccount(cnpEditAcc, getIdToEdit() , getSumWithdrawel());	
			        }
			        else if(SpendingAcc.isSelected()) {
			        	bank.WithdrawFromAccount(cnpEditAcc, getIdToEdit() , getAWithdrawel());	

			        }	        
					bank.save("bank.txt");
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}
	
	private void initialize() {
		panel.setLayout(new GridLayout(18, 1, 5, 10));
		panel.setBackground(Color.DARK_GRAY);
		AddAccount.setBackground(Color.ORANGE);
		EditAccount.setBackground(Color.ORANGE);
		DeleteAccount.setBackground(Color.ORANGE);
		ViewAllAccounts.setBackground(Color.MAGENTA);
		SavingAcc.setBackground(Color.ORANGE);
		SpendingAcc.setBackground(Color.ORANGE);
		Search.setBackground(Color.ORANGE);
		lb.setBackground(Color.ORANGE);
		lb.setForeground(Color.WHITE);
		lb2.setBackground(Color.ORANGE);
		AddSum.setBackground(Color.ORANGE);
		Withdraw.setBackground(Color.ORANGE);

		panel.add(lb);
		panel.add(cnp);
		panel.add(Search);
		panel.add(lb2);
		panel.add(SavingAcc);
		panel.add(deposit);
		panel.add(withdrawel);
		panel.add(SpendingAcc);
		panel.add(depotS);
		panel.add(drawelS);
		panel.add(AddSum);
		panel.add(Withdraw);
		panel.add(lb3);
		panel.add(AddAccount);
		panel.add(idToEdit);
		panel.add(EditAccount);
		panel.add(DeleteAccount);
		panel.add(ViewAllAccounts);
	}
	
	/**
	 * returns the cnp from the interface
	 * @return
	 */
	public String getCNP() {
		return cnp.getText();
	}
	
	/**
	 * return the deposit sum for saving account
	 * @return
	 */
	public float getSumDeposit() {
		return Float.parseFloat(deposit.getText());
	}
	
	/**
	 * return the sum to be withdrawn from a saving account
	 * @return
	 */
	public float getSumWithdrawel() {
		return Float.parseFloat(withdrawel.getText());
	}
	
	/**
	 * return the deposit sum for spending account
	 * @return
	 */
	public float getADeposit() {
		return Float.parseFloat(depotS.getText());
	}
	
	/**
	 * return the sum to be withdrawn from a spending account
	 * @return
	 */
	public float getAWithdrawel() {
		return Float.parseFloat(drawelS.getText());
	}
	/**
	 * returns the id of an account that we want to edit/delete/search for
	 * @return
	 */
	public int getIdToEdit() {
		return Integer.parseInt(idToEdit.getText());
	}
	
	/**
	 * returns the current id/global id 
	 * @return
	 */
	public int getGlobalId() {
		return globalId;
	}
	
	public static void main (String args[]) {
		new WindowAccount();
		
	}

}
