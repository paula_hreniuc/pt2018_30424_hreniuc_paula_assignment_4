package views;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * Created the view of the bank: person operations and account operations
 * @author Paula Hreniuc
 *
 */
public class ViewBank extends JFrame {

	private static final long serialVersionUID = 1L;
	
	JFrame frame  = new JFrame ("BANK");
	JPanel panel = new JPanel();
	JButton PersonOP = new JButton("Person Operations");
	JButton AccountOP = new JButton("Account Operations");
	
	/**
	 * creates frame and adds action listeners
	 */
	public ViewBank() {
		frame.add(panel);
		//frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 150);
		frame.setVisible(true);
		
		initialize();
		
		PersonOP.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					WindowPerson p = new WindowPerson();
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		AccountOP.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					WindowAccount p = new WindowAccount();
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
	}
	
	private void initialize() {
		panel.setLayout(new GridLayout(1, 2, 0, 0));
		panel.setBackground(Color.DARK_GRAY);
		PersonOP.setBackground(Color.DARK_GRAY);
		AccountOP.setBackground(Color.DARK_GRAY);
		PersonOP.setForeground(Color.ORANGE);
		AccountOP.setForeground(Color.ORANGE);
		panel.add(PersonOP);
		panel.add(AccountOP);
	}
	
	public static void main (String args[]) {
		new ViewBank();
	}

}
