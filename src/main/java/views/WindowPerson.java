package views;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Bank;
import model.Person;
import model.SpendingAccount;
/**
 * Creates the view for the Person operations
 * @author Paula Hreniuc
 *
 */
public class WindowPerson extends JFrame{

	private static final long serialVersionUID = 1L;
	JFrame frame  = new JFrame ("Person Operations");
	JPanel panel = new JPanel();
	JButton AddPerson = new JButton("Add New Person");
	JButton EditPerson = new JButton("Edit Person");
	JButton Delete = new JButton("Delete Person");
	JButton ViewAllPersons = new JButton("View All Persons");
	JLabel cnp = new JLabel("CNP:", JLabel.LEFT);
	JLabel firstName = new JLabel("First Name:", JLabel.LEFT);
	JLabel lastName = new JLabel("Last Name:", JLabel.LEFT);
	JLabel phone = new JLabel("Phone:", JLabel.LEFT);
	JTextField f1 = new JTextField();
	JTextField f2 = new JTextField();
	JTextField f3 = new JTextField();
	JTextField f4 = new JTextField();
	
	//public static List<Person> listPerson = new ArrayList<Person>();
	public String[][] allAdded = new String[100][4];
	public int i = 0;
	Bank bank = new Bank();
	private int Gid = 200;


	/**
	 * Creates the frame and the action listeners are added here 
	 */
	public WindowPerson() {
		
		frame.add(panel);
		//frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
		initialize();
		
		//add action listeners
		AddPerson.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					Person p = new Person(getCNP(), getFirstName(),getLastName(),getPhone());
					SpendingAccount acc = new SpendingAccount(p, 0, Gid);
					Gid -= 1;
					bank.addAccount(getCNP(), acc);
					bank.save("bank.txt");
					
					allAdded[i][0] = getCNP();
					allAdded[i][1] = getFirstName();
					allAdded[i][2] = getLastName();
					allAdded[i][3] = getPhone();
					System.out.println("Added " + allAdded[i][0] + allAdded[i][1] + allAdded[i][2] + allAdded[i][3]);
					i++;
					
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		EditPerson.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String editCNP = getCNP();

					for(int j = 0; j < i ; j++) {
						if(allAdded[j][0].equals(editCNP) ) {
							allAdded[j][1] = getFirstName();
							allAdded[j][2] = getLastName();
							allAdded[j][3] = getPhone();
						}
					}
					
					Person edit = new Person(getCNP(), getFirstName(), getLastName(), getPhone());
					bank.editPerson(editCNP, edit);
					bank.save("bank.txt");
					
					System.out.println("Edited person with cnp: " + editCNP);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		Delete.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String deleteCNP = getCNP();
				
					//int index = -1;
					int j;
					for( j = 0; j < i ; j++) {
						if(allAdded[j][0].equals(deleteCNP) ) {
							//index = j;
							allAdded[j][0] = " ";
							allAdded[j][1] = " ";
							allAdded[j][2] = " ";
							allAdded[j][3] = " ";
						}
					}
					bank.removePerson(deleteCNP);
					bank.save("bank.txt");
					
					System.out.println("deleted person with cnp: " + deleteCNP);
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
		
		ViewAllPersons.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					String[] columnsNames = {"cnp", "first name", "last name", "phone"};
					VJTable qui = new VJTable(allAdded, columnsNames);
					//qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					qui.setSize(600,200);
					qui.setVisible(true);
					
					bank.populate("bank.txt");
					String[] columnsNames2 = {"cnp", "first name", "last name", "phone"};
					String[][] all = bank.allPersons();
					VJTable qui2 = new VJTable(all, columnsNames2);
					qui2.setSize(600,200);
					qui2.setVisible(true);
					

				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}
	
	private void initialize() {
		panel.setLayout(new GridLayout(12, 1, 5, 10));
		panel.setBackground(Color.DARK_GRAY);
		AddPerson.setBackground(Color.ORANGE);
		EditPerson.setBackground(Color.ORANGE);
		Delete.setBackground(Color.ORANGE);
		ViewAllPersons.setBackground(Color.MAGENTA);
		cnp.setForeground(Color.ORANGE);
		firstName.setForeground(Color.ORANGE);
		lastName.setForeground(Color.ORANGE);
		phone.setForeground(Color.ORANGE);
		panel.add(cnp);
		panel.add(f1);
		panel.add(firstName);
		panel.add(f2);
		panel.add(lastName);
		panel.add(f3);
		panel.add(phone);
		panel.add(f4);
		panel.add(AddPerson);
		panel.add(EditPerson);
		panel.add(Delete);
		panel.add(ViewAllPersons);

	}
	
	/**
	 * returns from the interface the cnp of the person
	 * @return
	 */
	public String getCNP() {
		return f1.getText();
	}
	
	/**
	 * returns from the interface the first name of the person
	 * @return
	 */
	public String getFirstName() {
		return f2.getText();
	}
	
	/**
	 * returns from the interface the last name of the person
	 * @return
	 */
	public String getLastName() {
		return f3.getText();
	}
	
	/**
	 * returns the phone of the person from the interface
	 * @return
	 */
	public String getPhone() {
		return f4.getText();
	}
	
	public static void main (String args[]) {
		WindowPerson w = new WindowPerson();

		//Person p = new Person("1234567891234", "paula", "hre", "0745636627");

	}
}
