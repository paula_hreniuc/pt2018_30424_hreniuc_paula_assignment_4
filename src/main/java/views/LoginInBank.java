package views;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * This class represents the log in window.
 * @author Paula Hreniuc
 *
 */
public class LoginInBank extends JFrame {

	private static final long serialVersionUID = 1L;
	
	JFrame frame  = new JFrame ("BANK");
	JPanel panel = new JPanel();
	JButton LogIn = new JButton("Log in "); 
	JTextField user = new JTextField("");
	JPasswordField pass = new JPasswordField("");
	JLabel lb = new JLabel("USER : ", JLabel.RIGHT);
	JLabel lb2 = new JLabel("PASSWORD : ", JLabel.RIGHT);
	JLabel lb3 = new JLabel();


	/**
	 * Creates the frame and adds action listeners
	 */
	public LoginInBank () {
		frame.add(panel);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(500, 200);
		frame.setVisible(true);
		
		initialize();
		
		LogIn.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent event){

				try{
					//if pass and user correct
					String user = getusername() ;
					String pass = getpass();
					System.out.println(user + " " + pass);
					
					if (user.equals("paula")  &&  pass.equals("pass" )) {
						ViewBank p = new ViewBank();
					}
					else {
						JOptionPane.showMessageDialog(frame,"User or Password not correct", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
				catch(Exception e){
					System.out.println(e.getMessage());
				}
			}
		});
	}
	
	private void initialize() {
		panel.setLayout(new GridLayout(3, 4, 0, 0));
		panel.setBackground(Color.DARK_GRAY);
		LogIn.setBackground(Color.DARK_GRAY);
		LogIn.setForeground(Color.ORANGE);
		lb.setBackground(Color.DARK_GRAY);
		lb2.setBackground(Color.DARK_GRAY);
		lb.setForeground(Color.ORANGE);
		lb2.setForeground(Color.ORANGE);
		
		panel.add(lb);
		panel.add(user);
		panel.add(lb2);
		panel.add(pass);
		panel.add(lb3);
		panel.add(LogIn);
	}
	
	/**
	 * returns the username
	 * @return
	 */
	public String getusername() {
		return user.getText();
	}
	
	/**
	 * returns the passward
	 * @return
	 */
	public String getpass() {
		return pass.getText();
	}
	
	public static void main (String args[]) {
		new LoginInBank();
	}
	

}
