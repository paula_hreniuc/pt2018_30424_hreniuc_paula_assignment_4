package views;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Person;
/**
 * Creates the view of the JTabels 
 * @author Paula Hreniuc
 *
 */
public class VJTable extends JFrame {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param all
	 * @param columnsNames
	 */
	public VJTable (String[][] all, String[] columnsNames) {
		JTable table = new JTable(all,columnsNames);		
		setLayout(new FlowLayout());
		table.setPreferredScrollableViewportSize(new Dimension(500, 100));
		table.setFillsViewportHeight(true);

		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
		
		final ListSelectionModel model = table.getSelectionModel();
		model.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if(! model.isSelectionEmpty()) {
					//get selected row
					int selectedRow = model.getMinSelectionIndex();
					JOptionPane.showMessageDialog(null, "Selected Row " + selectedRow);
				}
			}
		});
	}
	
	public VJTable (List<Person> listP, String[] columnsNames) {
		String[][] all = new String[100][4];
		int j = 0;
		for (Person element : listP) {
		    	all[j][0] = element.getCNP();
		    	all[j][1] = element.getFirstName();
		    	all[j][2] = element.getLastName();
		    	all[j][3] = element.getPhone();
		    	j++;
		    
		}
		
		JTable table = new JTable(all,columnsNames);		
		setLayout(new FlowLayout());
		table.setPreferredScrollableViewportSize(new Dimension(500, 100));
		table.setFillsViewportHeight(true);

		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane);
	}
	
	
	public static void main(String[] args) {
		String[] columnsNames2 = {"id", "name", "price", "quantity"};
		String[][] all = {{"1", "Ana", "Bucium", "0763626616"},{"1", "Ana", "Plopilor", "0763626616"}};

		VJTable qui = new VJTable(all, columnsNames2);
		qui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		qui.setSize(600,200);
		qui.setVisible(true);
		qui.setTitle("Jtable");
	}
}
