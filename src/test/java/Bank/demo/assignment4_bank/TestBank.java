package Bank.demo.assignment4_bank;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

public class TestBank {

	    
    @Test
    public void TestAddAccount() {
    	Bank b = new Bank();
		b.populate("bank.txt");
        Person p = new Person("2941711125781", "Hreniuc", "Paula", "0745352271");
        SpendingAccount sa = new SpendingAccount(p, 1000, 6);
        SavingAccount sv = new SavingAccount(p, 5000, 8);
        SavingAccount sv2 = new SavingAccount(p, 5000, 10);
        
        b.addAccount("2941711125781", sa);
        b.addAccount("2941711125781", sv);
        b.addAccount("2941711125781", sv2);
    }
    
    @Test
    public void TestSearch() {
    	Bank b = new Bank();
		b.populate("bank.txt");
    	 ArrayList<Account> ac;
    	    ac = b.search("2941611125781");
    	    if (ac != null){
    	    	for(Account a: ac){
    	        	System.out.println(a.toString());
    	        }
    	    }
    	    else
    	    	System.out.println("Person not in bank");
    }
   
	@Test
	public void TestAddAccounts() {
		Bank b = new Bank();
		b.populate("bank.txt");
		
		Person p1 = new Person("5941711125781","Ana", "Pop", "0754636672");
		SavingAccount a = new SavingAccount(p1, 8000, 1);
		b.addAccount("5941711125781", a); 

		SavingAccount a2 = new SavingAccount(p1, 82900, 2);
		b.addAccount("5941711125781", a2); 
		
		Person p2 = new Person("1234567887234","AnaMaria", "Mariginean", "0754556672");
		SavingAccount acc = new SavingAccount(p2, 8000, 3);
		b.addAccount("1234567887234", acc);
		
		Person p3 = new Person("1231231231233", "Aurica", "Florea", "07445637721" );
		SpendingAccount a3 = new SpendingAccount(p3, 4579, 4);
		b.addAccount("1231231231233", a3);
		
	    b.save("bank.txt");      
	}
	

	@Test
	public void TestPrintAccounts() {
		Bank b = new Bank();
		b.populate("bank.txt");
		System.out.println("printing");
		b.getAccounts("2941711125781");
	    b.getAccounts("1234567887234");
		b.getAccounts("1231231231233");
		b.getAccounts("5941711125781");
	}
	

	@Test
	public void TestSearchForPerson() {
		Bank b = new Bank();
		b.populate("bank.txt");
		System.out.println("search");
		 ArrayList<Account> ac2;
	     ac2 = b.search("2941611125781");
	     if (ac2 != null){
	     	for(Account f: ac2){
	         	System.out.println("serached" + f.toString());
	         }
	     }
	     else
	     	System.out.println("Person not in bank");
	     
	     ArrayList<Account> ac4;
	     ac4 = b.search("1234567887234");
	     if (ac4 != null){
	     	for(Account f: ac4){
	         	System.out.println("serached for " + f.getPerson());// + f.toString());
	         	Person apers = f.getPerson();
	         	System.out.println("serached for person !!! " + ac4.get(0).getPerson().getCNP() + " " +apers.getCNP() + apers.getFirstName());
	         }
	     }
	     else
	     	System.out.println("Person not in bank"); 
	}
	    
     @Test
     public void TestEditAccount() {
    	 Bank b = new Bank();
 		 b.populate("bank.txt");
 	     System.out.println("\nedit\n");
 	     b.editAccount("2941711125781",8, 1863);
 	     b.getAccounts("2941711125781");
     }
     
     @Test
     public void TestRemoveAccount() {
    	 Bank b = new Bank();
 		 b.populate("bank.txt");
      	System.out.println("removed");
        b.removeAccount("2941711125781", 6);//sa
        b.getAccounts("2941711125781");
     }

    
    
}
